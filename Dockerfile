FROM python:3.7-slim
WORKDIR /app

# Install poetry
RUN pip install poetry --no-cache-dir && poetry config virtualenvs.create false

# Copy in the config files
COPY ./pyproject.toml ./poetry.lock ./

# Install dependencies
RUN poetry install

CMD ["sleep", "9999"]
