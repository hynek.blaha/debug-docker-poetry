# ABOUT
### (MINIMAL WORKING EXAMPLE)
I have been building Docker images using Poetry with Python packages from internal PyPI registry. As our projects are in private GitLab repository and the internal packages are not top-secret, we are storing the poetry credentials directly in the poetry source URL in `pyproject.toml`.

On **2022-08-24**, all our Docker builds started failing while installing internal package:

```shell
• Installing til-bigquery (0.3.4)

  HTTPError

  401 Client Error: Unauthorized for url: https://gitlab.com/api/v4/projects/38869805/packages/pypi/files/7a4731d831d4b37262481002271e359f96017570e9480ef16c89489e0b41252f/til_bigquery-0.3.4-py3-none-any.whl#sha256=7a4731d831d4b37262481002271e359f96017570e9480ef16c89489e0b41252f

  at /usr/local/lib/python3.9/site-packages/requests/models.py:1021 in raise_for_status
      1017│                 f"{self.status_code} Server Error: {reason} for url: {self.url}"
      1018│             )
      1019│ 
      1020│         if http_error_msg:
    → 1021│             raise HTTPError(http_error_msg, response=self)
      1022│ 
      1023│     def close(self):
      1024│         
      1025│         called the underlying ``raw`` object must not be accessed again.

```

# What I found weird:
- Docker build fails even when I retry deploy job, that successfully passed a few days ago.
- Considering the issue might have been caused by unpinned minor version of Docker base image `python:3.7-slim` or Poetry, I used older versions but got the same result.
- I compared the build logs of previously successful build `build_success.log` (**8/22/22, 3:00 PM**) and the same build retry `build_fail.log` (**8/24/22, 6:00 AM**) and found both use the same poetry wheel `poetry-1.1.15-py2.py3-none-any.whl`.

# It still works as before on my machine, but fails in Docker.

**How to reproduce:**
1) Localhost - OK
```shell
git clone git@gitlab.com:hynek.blaha/debug-docker-poetry.git
poetry install
```
2) Docker - FAIL
```shell
git clone git@gitlab.com:hynek.blaha/debug-docker-poetry.git
docker build .
```